<p align="center"><a href="https://github.com/anuraghazra/github-readme-stats">
  <img align="center" src="https://github-readme-stats.vercel.app/api?username=r3mcos3&show_icons=true&theme=tokyonight" />
</a></p>

<p align="center"><a href="https://wakatime.com/@r3mcos3">
  <img align="center" width="400" height="300" src="https://wakatime.com/share/@r3mcos3/3e8eabe0-6dab-4358-826f-46c28cdeea19.svg"/>
</a>
<a href="https://wakatime.com/@r3mcos3">
  <img align="center" width="400" height="300" src="https://wakatime.com/share/@r3mcos3/c6d0e7bb-c9fe-48c7-8bf2-a66fee960e05.svg" />
</a></p>

<p align="center"><a href="https://wakatime.com/@r3mcos3">
  <img align="center" width="400" height="300" src="https://wakatime.com/share/@r3mcos3/aad91693-62d4-4a25-a498-12b4ab75a904.svg" />
</a>
<a href="https://wakatime.com/@r3mcos3">
  <img align="center" width="400" height="300" src="https://wakatime.com/share/@r3mcos3/53d8d942-5fd4-41e9-a426-72deb45ce0a0.svg" />
</a></p>

### 👷 Check out what I'm currently working on

- [r3mcos3/HomeAssistant](https://github.com/r3mcos3/HomeAssistant) - My Home Assistant Files
- [r3mcos3/Dotfiles](https://github.com/r3mcos3/Dotfiles) - My Personal Dotfiles
- [r3mcos3/esphome-nodes](https://github.com/r3mcos3/esphome-nodes) - Yaml files of my Esphome nodes. :computer:
- [hassio-addons/addon-ftp](https://github.com/hassio-addons/addon-ftp) - FTP - Home Assistant Community Add-ons
- [piitaya/lovelace-mushroom](https://github.com/piitaya/lovelace-mushroom) - Mushroom Cards - Build a beautiful dashboard easily 🍄
### 🌱 My latest projects

- [r3mcos3/Dotfiles](https://github.com/r3mcos3/Dotfiles) - My Personal Dotfiles
- [r3mcos3/HomeAssistant](https://github.com/r3mcos3/HomeAssistant) - My Home Assistant Files
- [r3mcos3/esphome-nodes](https://github.com/r3mcos3/esphome-nodes) - Yaml files of my Esphome nodes. :computer:
### 🔨 My recent Pull Requests

- [persistent history](https://github.com/zap-zsh/supercharge/pull/5) on [zap-zsh/supercharge](https://github.com/zap-zsh/supercharge)
- [Fix typo in  documentation](https://github.com/hassio-addons/addon-ftp/pull/125) on [hassio-addons/addon-ftp](https://github.com/hassio-addons/addon-ftp)
- [Removed duplicate variable](https://github.com/piitaya/lovelace-mushroom/pull/703) on [piitaya/lovelace-mushroom](https://github.com/piitaya/lovelace-mushroom)
- [:arrow_up: Upgrades Tailscale to 1.24.2](https://github.com/hassio-addons/addon-tailscale/pull/92) on [hassio-addons/addon-tailscale](https://github.com/hassio-addons/addon-tailscale)
- [:arrow_up: Upgrades Tailscale to 1.22.4](https://github.com/hassio-addons/addon-tailscale/pull/91) on [hassio-addons/addon-tailscale](https://github.com/hassio-addons/addon-tailscale)
### ⭐ Recent Stars

- [reisxd/revanced-builder](https://github.com/reisxd/revanced-builder) - A NodeJS ReVanced builder
- [folke/lazy.nvim](https://github.com/folke/lazy.nvim) - 💤 A modern plugin manager for Neovim
- [kovidgoyal/kitty](https://github.com/kovidgoyal/kitty) - Cross-platform, fast, feature-rich, GPU based terminal
- [Schniz/fnm](https://github.com/Schniz/fnm) - 🚀 Fast and simple Node.js version manager, built in Rust
- [quickemu-project/quickemu](https://github.com/quickemu-project/quickemu) - Quickly create and run optimised Windows, macOS and Linux desktop virtual machines.
